<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('item_name');
            $table->mediumText('item_details')->nullable();
            $table->integer('item_budget')->nullable();
            $table->integer('requester');
            $table->dateTime('due_date')->nullable();
            $table->integer('status');
            $table->string('type');
            $table->dateTime('request_date');
            $table->integer('approver')->nullable();;
            $table->dateTime('approved_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_items');
    }
}
