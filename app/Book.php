<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	public function user(){
		return $this->belongsTo('App\User');
	}

	public function bookcat(){
		return $this->belongsTo('App\BookCategory','book_cat','id');
	}

	public function booklog(){
		return $this->hasMany('App\Booklogs');
	}
}
