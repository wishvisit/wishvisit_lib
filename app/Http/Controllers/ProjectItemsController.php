<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;
use App\ProjectItems;
use Carbon\Carbon;
use Auth;

class ProjectItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('project.createItem');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'item_name'         => 'required',
            'item_details'      => 'required',
            'type'              => 'required',
            'due_date'          => 'required_if:type,expense',
            'item_budget'       => 'required',
            'requester'         => 'required',
            'project_id'        => 'required',
        ],[
            'project_id.required'   => 'no project id someting wrong',
            'requester.required'    => 'no requester id someting wrong',
        ]
    
    );
        // New project
        $msg = "new income added";
        $projectItem = new Projectitems;
        $projectItem->project_id        = $request->input('project_id');
        $projectItem->item_name         = $request->input('item_name');
        $projectItem->item_details      = $request->input('item_details');
        $projectItem->type              = $request->input('type');
        $projectItem->item_budget       = $request->input('item_budget');
        $projectItem->requester         = $request->input('requester');
        if($request->input('due_date')){
            $projectItem->due_date          = date('Y-m-d', strtotime($request->input('due_date')));
            $msg = "new request added";
        }
        $projectItem->request_date      = Carbon::now()->format('Y-m-d');   
        $projectItem->save();

        return redirect('/project/project-item/create')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = ProjectItems::find($id);
        //$lastest_user = Booklogs::where('book_id',$id)->orderBy('id','desc')->first();
        
        
        $data = array(
            'projectItem'   => $project,
            //'lastest_user'  => $lastest_user
        );
        return view('project.showItem')->with($data);
    }

    /**
     * Display project item by filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function filter(request $request)
    {
        $project    = $request->query('project')?$request->query('project'): '%';
        $start_date = $request->query('start')?$request->query('start'): '2019/01/01';
        $end_date   = $request->query('end')?$request->query('end'):'2219/01/01';
        $status     = $request->query('status')?$request->query('status'):'%';
        $type       = $request->query('type')?$request->query('type'):'%';
        $project_items = ProjectItems::whereBetween('created_at',[$start_date,$end_date])->Status($status)->Type($type)->ofProject($project)->orderBy('created_at','desc')->paginate(50);
        //$lastest_user = Booklogs::where('book_id',$id)->orderBy('id','desc')->first();
        $projectlist= Projects::get();
        
        
        $data = array(
            'project_items' => $project_items,
            'start'         => $start_date,
            'end'           => $end_date,
            'type'          => $type,
            'status'        => $status,
            'project'       => $project,
            'projectlist'   => $projectlist,
        );
        return view('project.showItemFiltered')->with($data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $request,$id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function approve($id){
        if(!$id){
            return redirect()->back()->with('error','no id');
        }
        $ProjectItem = ProjectItems::find($id);
        $ProjectItem->status = 1;
        $ProjectItem->approver =  Auth::id();
        $ProjectItem->save();
        return redirect()->back()->with('success','item approved');
    }

    public function decline($id){
        if(!$id){
            return redirect()->back()->with('error','no id');
        }
        $ProjectItem = ProjectItems::find($id);
        $ProjectItem->status = 2;
        $ProjectItem->approver =  Auth::id();
        $ProjectItem->save();
        return redirect()->back()->with('success','item decline');
    }

}
