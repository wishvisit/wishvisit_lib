<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Booklogs;
use App\BookCategory;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $books = Book::paginate(5); 
        return view('pages.index')->with('books', $books);
    }

    public function category(request $request)
    {   
        $category = $request->input('category');
        if(!$category == null){
            $books      = Book::where('book_cat',$category)->paginate(15);    
        }        
        else{
            $books      = Book::paginate(5);
        }
        $categories = BookCategory::all();
        $data       = array(
            'books'        => $books,
            'categories'   => $categories
        );
        return view('book.category')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = BookCategory::all();
        return view('book.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'book_name' => 'required',
            'book_desc' => 'required',
            'book_cat' => 'required',
        ]);
        // New book
        $book = new Book;
        $book->book_name = $request->input('book_name');
        $book->book_desc = $request->input('book_desc');
        $book->book_cat =  $request->input('book_cat');
        $book->book_status ='available';
        $book->save();

        return redirect('/book')->with('success', 'new book added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);
        $lastest_user = Booklogs::where('book_id',$id)->orderBy('id','desc')->first();
        $data = array(
            'book'          => $book,
            'bookslog'      => $book->booklog,
            'lastest_user'  => $lastest_user
        );
        return view('book.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $request,$id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    public function borrow($id)
    {
       //check if avaiavle        
        $is_avialble =  Book::where('id',$id)
                            ->where('book_status','availble');
        if(!$is_avialble){
             return redirect()->back()->withErrors('error','Book are not availble');
        }

        // change status to not available
        $book = Book::find($id);
        $book->book_status = 'not available';
        $book->save();

        //add data to borrow log
        $book_log = new Booklogs;
        $book_log->book_id = $id;
        $book_log->user_id = auth()->user()->id;
        $book_log->borrow_date = date('Y-m-d H:i:s');
        $book_log->due_date = date_add(date_create(),date_interval_create_from_date_string("7 days"));
        $book_log->return_date = null;
        $book_log->save();

        return redirect()->back()->with('success','Book borrowed');
        //return Redirect::back()->withSuccess('Book borrowed');
    }

    public function return($id)
    {
        //change status to available
        $book = Book::find($id);
        $book->book_status = 'available';
        $book->save();

        //add return date to borrow log
        $book_log = Booklogs::where('book_id',$id)->first();
        $book_log->return_date = date('Y-m-d H:i:s');
        $book_log->save();    

        return redirect()->back()->with('success','Book return');
        //return Redirect::back()->withSuccess('Book return');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
