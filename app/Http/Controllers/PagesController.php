<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;

class PagesController extends Controller
{
   public function index(){
        $title = 'Home page';
        $projects = Projects::paginate(5); 
    	return view('pages.index')->with('projects', $projects);
    }

   public function category(){
    	$title = 'Category Page';
    	return view('pages.category')->with('title', $title);
    }
}
