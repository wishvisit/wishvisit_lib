<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(){
    	$title = 'Category Page';
    	return view('pages.category')->with('title', $title);
    }
}
