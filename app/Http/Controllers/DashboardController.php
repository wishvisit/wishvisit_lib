<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booklogs;
use App\Projects;
use App\ProjectItems;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $project_items = ProjectItems::where('status',0)->get();
        $data = array(
            'project_items' => $project_items,
            'user_id'       => $user_id
        );
        return view('dashboard')->with($data);
    }
}
