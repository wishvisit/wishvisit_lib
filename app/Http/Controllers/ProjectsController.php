<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Projects;
use App\ProjectItems;
use App\User;
use Auth;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'project_name' => 'required',
            'project_desc' => 'required',
        ]);
        // New project
        $project = new Projects;
        $project->project_name = $request->input('project_name');
        $project->project_description = $request->input('project_desc');
        $project->save();

        //assign user to project owner
        User::find(Auth::id())->assign_in_projects()->attach([$project->id => ['role' =>'user']]);
            
        return redirect("/project/$project->id")->with('success', 'new project created');
    }

    /**
     * Display the specified resource.
     *
     *   @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Projects::find($id);
        //$lastest_user = Booklogs::where('book_id',$id)->orderBy('id','desc')->first();
        $users = array();
        $users_in_project       = $project->users_in_project;
        foreach($users_in_project as $user){
            $users[] = $user->id;
        }

        $total_expense      = ProjectItems::OfProject($id)->Type('expense')->Status(1)->sum('item_budget');
        $pending_expense    = ProjectItems::OfProject($id)->Type('expense')->Status(0)->sum('item_budget');
        $income             = ProjectItems::OfProject($id)->Type('income')->Status(1)->sum('item_budget');

        $data = array(
            'project'       => $project,
            'project_items' => $project->project_item_list,
            'user_list'     => $users,
            'total_expense' => $total_expense,
            'pending_expense'  => $pending_expense,
            'income'        => $income,
            //'lastest_user'  => $lastest_user
        );
        return view('project.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $request,$id)
    {
        $project = Projects::find($id);
        $users_in_project       = $project->users_in_project;
        $users_not_in_project   = User::whereDoesntHave('assign_in_projects', function (Builder $query)use ($id) {
            $query->where('project_id', $id);
        })->get();
        $data = array(
            'project'               => $project,
            'users_in_project'      => $users_in_project,
            'users_not_in_project'  => $users_not_in_project,
        );
        return view('project.edit')->with($data);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add_user($project_id,$user_id){
        $is_assigned = User::find($user_id)->assign_in_projects->contains($project_id);
        if(!$is_assigned){
            User::find($user_id)->assign_in_projects()->attach([$project_id => ['role' =>'user']]);
            return redirect()->back()->with('success','user assigned');
        }else{
            return redirect()->back()->with('error','already assigned');
        }
    }

    public function remove_user($project_id,$user_id){
        $is_assigned = User::find($user_id)->assign_in_projects->contains($project_id);
        if($is_assigned){
            User::find($user_id)->assign_in_projects()->detach($project_id);
            return redirect()->back()->with('success','user removed');
        }else{
            return redirect()->back()->with('error','user is not member of project');
        } 
    }
}
