<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    public $timestamps = true;

    public function project_item_list(){
        return $this->hasMany('App\ProjectItems','project_id','id');
    }
    public function users_in_project(){
        return $this->belongsToMany('App\User','project_users','project_id','user_id')->withPivot('role')->where('role','user');
    }
    
}
