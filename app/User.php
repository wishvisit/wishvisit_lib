<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function assign_in_projects(){
        return $this->belongsToMany('App\Projects','project_users','user_id','project_id')->withTimestamps();
    }
    public function booklog(){
        return $this->hasMany('App\Booklogs');
    }
    public function roles(){
        return $this->belongsToMany(Roles::class,'users_roles','user_id','role_id');
    }
    // scope

    public function scopeInthisProject($query,$project_id) {
        return $query->whereBetween('created_at', ['2017-11-08 00:00:00', '2018-11-08 00:00:00']);
    }
    

    // helper function
    /**
    * @param string|array $roles
    */
    public function checkRole($roles)
    {
    if (is_array($roles)) {
        return $this->hasAnyRole($roles) || false; // abort(401, 'This action is unauthorized.');
    }
    return $this->hasRole($roles) ||  false;
    }

    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role)
    {
        
    return null !== $this->roles()->where('name', $role)->first();
    }

    /**
    * Check multiple roles
    * @param array $roles
    */
    public function hasAnyRole($roles)
    {
    return null !== $this->roles()->whereIn('name', $roles)->first();
    }
}
