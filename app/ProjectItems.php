<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectItems extends Model
{
    
    public function requestBy(){
        return $this->belongsTo('App\User','requester','id');
    }

    public function approveBy(){
        return $this->belongsTo('App\User','approver','id');
    }
    public function project(){
        return $this->belongsTo('App\Projects','project_id','id');
    }
    
    // scope

    public function scopeStatus($query,$status){
        return $query->where('status','like',$status);
    }
    

    public function scopeType($query,$type){
        return $query->where('type','like',$type);
    }
    public function scopeOfProject($query,$project_id){
        return $query->where('project_id','like',$project_id);
    }
}
