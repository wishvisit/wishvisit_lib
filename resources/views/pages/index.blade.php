@extends('layouts.header')

@section('content')
	
    <h1>Welcome to CPoint</h1>
    @guest
    <p>Please login or register to add new project in dashboard</p>
    @endguest
    {{-- filter project--}}
        {{--<a href="#"><button type="button" class="btn btn-primary" >View all project</button></a>
        <a href="#"><button type="button" class="btn btn-primary" >sort by date</button></a>
        <a href="#"><button type="button" class="btn btn-primary" >sort by date</button></a>--}}
    {{-- filter date--}}
	 <div class="d-flex">
		<h4 class="bg-danger text-white m-0 p-3">All Projects</h4>
		<span class="m-0 triangle triangle-1"></span>
	</div>
    @if($projects)
        @foreach($projects as $project)
        <div class="bg-red mt-3 mb-3 rounded p-3">
            <a href="/project/{{$project->id}}">
            <h3> {{$project->project_name}}</h3>
            <small>created on {{$project->created_at}}</small>
            @if($project->project_description)
                <p class="" >{{$project->project_description}}</p>
            @endif
            </a>
        </div>
        @endforeach
        {{$projects->links()}}
    @else
        <p>No project found</p>
    @endif
@endsection