@extends('layouts.header')

@section('content')

<div class="col-12">
    <div class="row">
        <div class="col-md-12">
            <div class="panel bg-red">
                <div class="panel-heading"> 
                <h1 class="m-0">Welcome :  {{auth()->user()->name}}</h1>
               
                </div>
                
                
                    @if (session('status'))
                       <div class="panel-body">
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                         </div>
                    @endif
               
            </div>
        </div>
    </div>
    {{--if admin --}}
    <section class="row">
        <div class="col-md-12">
            <div class="d-flex">
				<h4 class="bg-danger text-white m-0 p-3">Pending Request</h4>
				<span class="m-0 triangle triangle-1"></span>
			</div>
            <table class="table table-striped ">
                <thead>
                    <th scope="col">Project</th>
                    <th scope="col">Name</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Reqeust by</th>
                    <th scope="col">Reqeust date</th>
                    <th scope="col">Need date</th>
                    <th scope="col">type</th>
                    @if(auth()->user()->checkRole('manager'))
                    <th scope="col">Action</th>
                    @endif
                </thead>
                <tbody>
                @if($project_items)
                    @foreach($project_items as $project_item)
                    <tr>
                        <td><a href="/project/{{$project_item->project->id}}">{{$project_item->project->project_name or "-"}}</a></td>
                        <td><a href="/project/project-item/{{$project_item->id}}">{{$project_item->item_name or "-"}}</a></td>
                        <td>{{$project_item->item_budget or "-"}}</td>
                        <td>{{$project_item->requestBy->name or "-"}}</td>
                        <td>{{\Carbon\Carbon::parse($project_item->request_date)->format('d/m/Y') }}</td>
                        <td>{{\Carbon\Carbon::parse($project_item->due_date)->format('d/m/Y') }}</td>
                        <td>{{$project_item->type}}</td>
                        @if(auth()->user()->checkRole('manager'))
                        <td class="dropdown">
                            <button href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" style="top:initial">
                                <li>
                                    <a class="dropdown-item " href="/project/project-item/{{$project_item->id}}/approve">Approve</a>
                                    <a class="dropdown-item" href="/project/project-item/{{$project_item->id}}/decline">Decline</a>
                                </li>
                            </ul>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">No any pendding request</td>
                    </tr>
                @endif
            </tbody>
            </table>
        </div>
    </section>
    {{--if admin --}}
</div>
@endsection
