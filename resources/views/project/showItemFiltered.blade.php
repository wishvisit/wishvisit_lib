@extends('layouts.header')

@section('content')
	<section class="header row">
			<div class="d-flex">
				<h4 class="bg-danger text-white m-0 p-3">Filtered</h4>
				<span class="m-0 triangle triangle-1"></span>
			</div>
			<hr class="line mb-4">
			<section class="col-12 border p-3">
            @if($project)
                <div class="date col-12 col-md-2" >
                    <div id="sandbox" class="form-group">
                            <label for="start">Project</label>
                            <select class="form-control form-filter" id="project" data-type="project">
                                <option value="%">All</option>
                                @forelse($projectlist as $project)
                                <option value="{{$project->id}}">{{$project->project_name}}</option>
                                @empty
                                    
                                @endforelse
                            </select>
                        </div>
                    <!--<p>{{($start=="2019/01/01"?'The Begining':\Carbon\Carbon::parse($start)->format('d/m/Y'))}}-->
                </div>
            @endif
            @if($start)
                <div class="date col-12 col-md-2" data-date-format="dd.mm.yyyy">
                    <div id="sandbox" class="form-group">
                            <label for="start">From Date</label>
                            <input id="start" type="text" class="form-control datepicker form-filter" value="" data-type="start" autocomplete="off">
                        </div>
                    <!--<p>{{($start=="2019/01/01"?'The Begining':\Carbon\Carbon::parse($start)->format('d/m/Y'))}}-->
                </div>
            @endif
            @if($end)
                <div class="date col-12 col-md-2" data-date-format="dd.mm.yyyy">
                    <div id="sandbox" class="form-group">
                        <label for="end">To Date</label>
                        <input id="end" type="text" class="form-control datepicker form-filter" value="" data-type="end" autocomplete="off">
                    </div>
                    <!--<p>{{($end=="2219/01/01"?'Latest':\Carbon\Carbon::parse($end)->format('d/m/Y'))}}-->
                </div>	
            @endif
            @if($type)

                <div class="form-group col-12 col-md-2">
                    <label for="type">Type:</label>
                    <select class="form-control form-filter" id="type" data-type="type">
                        <option value="%">All</option>
                        <option value="expense">Expense</option>
                        <option value="income">Income</option>
                    </select>
                </div>
                <!--<p>{{($type=="%"?'All':$type)}}</p>-->
                
            
            @endif
            @if($status)
                <div class="form-group col-12 col-md-2">
                    <label for="status">Status:</label>
                    <select class="form-control form-filter" id="status" data-type="status">
                        <option value="%">All</option>
                        <option value="0">Pending</option>
                        <option value="1">Approved</option>
                        <option value="2">Declined</option>
                    </select>
                </div>
                <!--<p>{{($status=="%"?'All':$status)}}</p>-->
            @endif
            
        <a href="?" class="btn btn-danger text-white output" >Submit</a>
		</section>
	</section>
	@if($project_items)
	<section class="row">
        <div class="col-md-12">
           
            <table class="table table-striped mt-3">
                <thead>
                    <th>Project</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Reqeust by</th>
                    <th>Reqeust date</th>
                    <th>Need date</th>
                    <th>type</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @if($project_items)
                    @foreach($project_items as $project_item)
                    <tr>
                        <td><a href="/project/{{$project_item->project->id}}">{{$project_item->project->project_name or "-"}}</a></td>
                        <td><a href="/project/project-item/{{$project_item->id}}">{{$project_item->item_name or "-"}}</a></td>
                        <td>{{$project_item->item_budget or "-"}}</td>
                        <td>{{$project_item->requestBy->name or "-"}}</td>
                        <td>{{\Carbon\Carbon::parse($project_item->request_date)->format('d/m/Y') }}</td>
                        <td>{{\Carbon\Carbon::parse($project_item->due_date)->format('d/m/Y') }}</td>
                        <td>{{$project_item->type}}</td>
                        <td class="dropdown">
                            <button href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" style="top:initial">
                                <li>
                                    <a class="dropdown-item " href="/project/project-item/{{$project_item->id}}/approve">Approve</a>
                                    <a class="dropdown-item" href="/project/project-item/{{$project_item->id}}/decline">Decline</a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">No any pendding request</td>
                    </tr>
                @endif
            </tbody>
            </table>
            {{ $project_items->appends($_GET)->links() }}
        </div>
    </section>
	@else
		<section>
			no project data
		</section>
	@endif
@endsection
@push('footer-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
var date = new Date();
var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

var optSimple = {
  format: 'yyyy-mm-dd',
  todayHighlight: true,
  orientation: 'bottom right',
  autoclose: true,
  container: '#sandbox'
};

var optComponent = {
  format: 'mm-dd-yyyy',
  container: '#datePicker',
  orientation: 'auto top',
  todayHighlight: true,
  autoclose: true
};

$(document).ready(function(){
    var params  = {};
    $('.form-filter').change(function(){
        var type    = $(this).attr('data-type');
        var value   = $(this).val();
        var _href   = $("a.output").attr("href");
        params[type] = $(this).val();
        $("a.output").attr("href", "?" + $.param(params));
    })
 
});


// SIMPLE
$( '.datepicker' ).datepicker( optSimple );


// ===================================

$( '#simple, #datePicker' ).datepicker( 'setDate', today );		
</script>

@endpush
