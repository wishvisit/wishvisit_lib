@extends('layouts.header')

@section('content')
	@if (session('status'))
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			</div>
		</div>
	@endif
	<a href="/" class="btn btn-default">Back</a>
	<h1>Create new Project</h1>
	{!! Form::open(['action' => 'ProjectsController@store', 'method' => 'POST']) !!}
		<div class="form-group">
			{{Form::label('project_name','Project Name')}}
			{{Form::text('project_name','',['class' => 'form-control', 'placeholder' => 'Project Name'])}}
		</div>
		<div class="form-group">
			{{Form::label('project_desc','Project Desciption')}}
			{{Form::textarea('project_desc','',['class' => 'form-control', 'placeholder' => 'Project Desciption'])}}

		</div>
		{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
	{!! Form::close() !!}
@endsection