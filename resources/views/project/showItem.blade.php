@extends('layouts.header')

@section('content')
	@if($projectItem)
		<section class="project-details">
			<?php $text = "";?>
			<div class="d-flex">
					<h4 class="bg-danger text-white m-0 p-3">หัวข้อ</h4>
					<span class="m-0 triangle triangle-1"></span>
				</div>
			<hr class="line">
			
			<div class="bg-red p-3">
			<h3>{{$projectItem->item_name or "-"}}</h3>
			<small>โพสต์เมื่อ : {{\Carbon\Carbon::parse($projectItem->request_date)->format('d/m/Y h:m:s')}}</small>
			<div class="bg-white p-3 mt-3">{{$projectItem->item_details or "-"}}</div>
			</div>
			
			<section class="table-detail mt-4 m-0">
				<div class="d-flex">
					<h4 class="bg-danger text-white m-0 p-3">รายละเอียด</h4>
					<span class="m-0 triangle triangle-1"></span>
				</div>
				<hr class="line">
				<div class="row m-0">
					<div class="col-3">Requestde by : </div>
					<div class="col-9">{{$projectItem->requestBy->name or "-"}}</div>
				</div>
				<div class="row m-0">
					<div class="col-3">Project Budget : </div>
					<div class="col-9">{{$projectItem->item_budget or "-"}}</div>
				</div>
				<div class="row m-0">
					<div class="col-3">Status :</div>
					<div class="col-9">
						@switch($projectItem->status )
							@case(1)
								{{"Approved"}}
								<?php $text = "Approved";?>
								@break
							@case(2)
								{{"Declined"}}
								<?php $text = "Declined";?>
								@break
							@default
								{{"Pending"}}
						@endswitch
					</div>
				</div>
				<div class="row m-0">
					@if($projectItem->status == 1 || $projectItem->status == 2 )
					<div class="col-3"> {{$text}} by: </div>
					<div class="col-9">
						@if($projectItem->approver)
						{{$projectItem->approveBy->name or "-"}}
						@else
						-
						@endif
					</div>
				</div>
				<div class="row m-0">
					<div class="col-3"> {{$text}} time : </div>
					<div class="col-9">{{\Carbon\Carbon::parse($projectItem->updated_at)->format('d/m/Y h:m:s')}} </div>
				</div>
				@if(auth()->user()->checkRole('manager'))
				<div class="row m-0">
					<div class="col-3">Change Status : </div>
					<div class="col-9">
						<button href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
							Action 
						</button>
						<ul class="dropdown-menu" style="top:initial">
							<li>
								<a class="dropdown-item " href="/project/project-item/{{$projectItem->id}}/approve">Approve</a>
								<a class="dropdown-item" href="/project/project-item/{{$projectItem->id}}/decline">Decline</a>
							</li>
						</ul></div>
				</div>
				@endif
			</section>
			@endif
		</section>
	@else
		<section>
			no project data
		</section>
	@endif
@endsection