@extends('layouts.header')

@section('content')

	@if($project)
		@if(in_array(auth()->user()->id,$user_list))
			 {{--check if memeber of project --}}
			 <div class="row">
				<div class="col-8 mt-3">
					<ul class="d-flex">
					<li><h4>สร้างโพสต์</h4></li>
					<li><a href="/project/project-item/create?project_id={{$project->id}}"><button type="button" class="ml-3 btn btn-dark text-white" ><span class="glyphicon glyphicon-plus txt-20"></span> Create request</button></a></li>
					</ul>
				</div>
				<div class="col-4 project-option m-0"><span>Setting</span>
					<ul class="project-option-menu">
						<li><a href="/project/{{$project->id}}/edit">Manage user</a></li>
					</ul>
				</div>
				</div>
			@endif
		<section class="project-details p-3">
			<div>
				<div class="project-name d-flex">
				<h4 class="bg-danger text-white m-0 p-3">Project Name : {{$project->project_name or "-"}}</h4>
				<span class="m-0 triangle triangle-1"></span>
				</div>
				
			</div>
			<hr class="line">
			<div class="col-12 p-3 bg-red">
			<small>โพสต์เมื่อ : {{$project->created_at or "-"}}</small>
			<p class="p-2 bg-white mt-3">{{$project->project_description or "-"}}</p>
			</div>
			

		</section>
		{{--<section> 
			<h3>filter</h3>
		</section>--}}
		<section class="p-3 mt-3 p-3">
			<div class="d-flex">
				<h4 class="bg-danger text-white m-0 p-3">Summary</h4>
				<span class="m-0 triangle triangle-1"></span>
			</div>
			<hr class="line">
			<div class="row m-0 bg-red">
				<div class="col-4">
					<h4>Expense</h4>
				</div>
				<div class="col-4">
					<h4>Pending</h4>
					<p>{{$pending_expense or "-"}}</p>
				</div>
				<div class="col-4">
					<h4>Confirmed Expense</h4>
					<p>{{$total_expense or "-"}}</p>
				</div>
			</div>
			<div class="row m-0" style="background: #fdf1f2;">
				<div class="col-4">
					<h4>Income</h4>
				</div>
				<div class="col-4">
					<h4>Pending</h4>
					<p>{{$pending_expense or "-"}}</p>
				</div>
				<div class="col-4">
					<h4>Confirmed Expense</h4>
					<p>{{$total_expense or "-"}}</p>
				</div>
			</div>
			
		</section>
		<section class="project-log p-3 mt-3">
			<div class="d-flex">
				<h4 class="bg-danger text-white m-0 p-3">Project Log</h4>
				<span class="m-0 triangle triangle-1"></span>
			</div>
			<hr class="line">
			<table class="table txt-12">
				<thead style="background: #fff;">
					<tr>
						<th scope="col"><img src="{{asset('images/icon/icon-18.png')}}" class="center"><p>ID</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-19.png')}}" class="center"><p>Name</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-20.png')}}" class="center"><p>Details</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-21.png')}}" class="center"><p>Type</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-22.png')}}" class="center"><p>Budget</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-23.png')}}" class="center"><p>Requester</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-24.png')}}" class="center"><p>Request Date</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-25.png')}}" class="center"><p>Status</p></th>
						<th scope="col"><img src="{{asset('images/icon/icon-26.png')}}" class="center"><p>Approved/Declined</p></th>
						@if(auth()->user()->checkRole('manager'))
						<th scope="col"><p>Action</p></th>
						@endif
					</tr>        			        	
				</thead>
				<tbody>
					@if($project_items)
						@foreach($project_items as $project_item)
						<tr>
							<th scope="row"><a href="/project/project-item/{{$project_item->id}}">{{$project_item->id}}</a></td>
							<td><a href="/project/project-item/{{$project_item->id}}">{{$project_item->item_name or "-"}}</a></td>
							<td>{{$project_item->item_details or "-"}}</td>
							<td>{{$project_item->type or "-"}}</td>
							<td>{{$project_item->item_budget or "-"}}</td>
							<td>{{$project_item->requestBy->name or "-"}}</td>
							<td>{{\Carbon\Carbon::parse($project_item->request_date)->format('d/m/Y')}}</td>
							<td>
							@switch($project_item->status )
								@case(1)
									{{"Approved"}}
									@break
								@case(2)
									{{"Declined"}}
									@break
								@default
									{{"Pending"}}
							@endswitch
							</td>
							@if($project_item->approver)
							<td>{{$project_item->approveBy->name or "-"}}</td>
							@else
							<td>-</td>
							@endif
							@if(auth()->user()->checkRole('manager'))
								@if($project_item->status == 0)
								<td class="dropdown">
									<button href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
										Action
									</button>
									<ul class="dropdown-menu" style="top:initial">
										<li>
											<a class="dropdown-item " href="/project/project-item/{{$project_item->id}}/approve">Approve</a>
											<a class="dropdown-item" href="/project/project-item/{{$project_item->id}}/decline">Decline</a>
										</li>
									</ul>
								</td>
								@else
								<td> <button href="#" class="dropdown-toggle btn btn-disable" disabled data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
										Action 
								</button></td>
								@endif
							@endif
						</tr>
						@endforeach        
					@endif
				</tbody>
			</table>
		</section>
	@else
		<section>
			no project data
		</section>
	@endif
	<style>
		.project-name {display: inline-block}
		.project-option {float: right;display: inline-block;margin-top: 20px;position: relative;}
		.project-option span {display: block;text-align: right;}
		.project-option:hover .project-option-menu {display: block;}
		.project-option-menu {display: none;list-style: none;background: white;padding: 20px;}
		
	</style>
@endsection