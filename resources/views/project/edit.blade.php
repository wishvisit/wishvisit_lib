@extends('layouts.header')

@section('content')
	@if (session('status'))
	<div class="panel panel-default">
		<div class="panel-body">
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
		</div>
	</div>
	@endif
	<a href="/" class="btn btn-default">Back</a>
	
	<section class="project-details">
		<div class="project-name">
		<h1>Manange : {{$project->project_name or "-"}}</h1>
		</div>
		<div class="well">{{$project->project_description or "-"}}</div>
	</section>
	<section>
		<h2>user</h2>
		<div>
			<h3>Current user in this project</h3>
			<dl class="user-list row">
			@forelse($users_in_project as $user)
				<dt class="col-sm-6">{{$user->name}}</dt>
				<dd class="col-sm-3">{{$user->pivot->role or "-"}}</dd>
				<dd class="col-sm-3">
					{!! Form::open(['action' => ['ProjectsController@remove_user',$project->id,$user->id], 'method' => 'POST']) !!}		            
						{{Form::submit('Remove', ['class' => 'btn btn-danger'])}}
					{!! Form::close() !!}
				</dd>
			@empty
				<p> no user</p>
			@endforelse
		</div>
		<div>
			<h3>Add user</h3>
			<dl class="user-list row">
			@forelse($users_not_in_project as $adduser)
				<dt class="col-sm-6">{{$adduser->name}}</dt>
				<dd class="col-sm-3">{{$adduser->role or "-"}}</dd>
				<dd class="col-sm-3">          
					 {!! Form::open(['action' => ['ProjectsController@add_user',$project->id,$adduser->id], 'method' => 'POST']) !!}		            
						{{Form::submit('Add', ['class' => 'btn btn-primary'])}}
					{!! Form::close() !!}
				</dd>
			@empty
				<p> all users are in project</p>
			@endforelse
			</dl>
		</div>
	</section>
	<style>
		dd {
			margin-bottom: 10px;
		}
	</style>
@endsection