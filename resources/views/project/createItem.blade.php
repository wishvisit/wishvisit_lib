@extends('layouts.header')

@section('content')
	<link href="{{ asset('css/jquery-ui.structure.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquery-ui.theme.min.css') }}" rel="stylesheet">
	@if (session('status'))
	<div class="panel panel-default">
		<div class="panel-body">
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
		</div>
	</div>
	@endif
	<a href="/project/{{app('request')->input('project_id')}}" class="btn btn-default">Back</a>
	<h1>Create Item Request</h1>
	{!! Form::open(['action' => 'ProjectItemsController@store', 'method' => 'POST']) !!}
		<div class="form-group">
			{{Form::label('item_name','Name')}}
			{{Form::text('item_name','',['class' => 'form-control', 'placeholder' => 'item Name'])}}
		</div>
		<div class="form-group">
			{{Form::label('item_details','Details')}}
			{{Form::textarea('item_details','',['class' => 'form-control', 'placeholder' => 'item Desciption'])}}

		</div>
		<div class="form-group">
			{{Form::label('due_date','Due date')}}
			{!! Form::text('due_date', null, ['class' => 'form-control','placeholder' => 'due date', 'id' => 'datepicker', 'autocomplete' => 'off']) !!}

		</div>
		<div class="form-group">
			{{Form::label('type','Type')}}
			{{Form::select('type', array('' => 'Select') + array('expense' => 'Expense', 'income' => 'Income'), ['class' => 'select-type'])}}
		
		</div>
		<div class="form-group">
			{{Form::label('item_budget','Request Budget')}}
			{{Form::number('item_budget','',['class' => 'form-control', 'placeholder' => 'Butget'])}}
			{{Form::hidden('project_id',app('request')->input('project_id'))}}
			{{Form::hidden('requester',Auth::id())}}
		</div>
		{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
	{!! Form::close() !!}	
	<div class="padding-area" style="padding-bottom:25px;">
	</div>
	@push('footer-scripts')
	<script>
		$(document).ready(function(){
			$('select#type option:first').attr('disabled', true);
		});

		$( function() {
			$( "#datepicker" ).datepicker();
		} );
	</script>
	@endpush
@endsection