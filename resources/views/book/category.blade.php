@extends('layouts.header')

@section('content')

    @if($categories)
    {!! Form::open(['action' => ['BookController@category'], 'method' => 'GET']) !!}          
        <div class="form-group">
            {{Form::label('book_cat','Book Category')}}
          <select class="custom-select" name="category">
             <option selected value="0">Choose...</option>
                @foreach($categories as $category)              
                    <option value="{{$category->id}}">{{$category->category_name}}</option>               
                @endforeach
          </select>
       
            {{Form::submit('Select', ['class' => 'btn btn-primary'])}}
        </div>
    {!! Form::close() !!}
    @endif

    <h1>Books</h1>
    @if($books)
        @foreach($books as $book)
        <div class="well">
            <h3><a href="/book/{{$book->id}}"> {{$book->book_name}}</a> <span class="badge badge-secondary">{{$book->bookcat->category_name}}</span></h3>
            <small>Added on {{$book->created_at}}</small>
            @if($book->book_status == 'available')
                <span class="badge badge-success" style="background: green">{{$book->book_status}}</span>
            @else
                <span class="badge badge-secondary">{{$book->book_status}}</span>
            @endif
        </div>
        @endforeach
        {{$books->links()}}
    @else
        <p>No book found</p>
    @endif
@endsection