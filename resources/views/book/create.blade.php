@extends('layouts.header')

@section('content')
	<a href="/book" class="btn btn-default">Back</a>
	<h1>Add new book</h1>
	{!! Form::open(['action' => 'BookController@store', 'method' => 'POST']) !!}
		<div class="form-group">
			{{Form::label('book_name','Book Name')}}
			{{Form::text('book_name','',['class' => 'form-control', 'placeholder' => 'Book Name'])}}
		</div>
		<div class="form-group">
			{{Form::label('book_desc','Book Desciption')}}
			{{Form::textarea('book_desc','',['class' => 'form-control', 'placeholder' => 'Book Desciption'])}}

		</div>
		<div class="form-group">
			{{Form::label('book_cat','Book Category')}}
          	<select class="custom-select" name="category">
             	<option selected value="0">Choose...</option>
                	@foreach($categories as $category)              
                    	<option value="{{$category->id}}">{{$category->category_name}}</option>               
               		 @endforeach
          	</select>
 
        </div>
		{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
	{!! Form::close() !!}
@endsection