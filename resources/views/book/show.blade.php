@extends('layouts.header')

@section('content')
		@if($book)
        <h1>{{$book->book_name}}</h1>
        <small>add date {{$book->created_at}}</small>
        <div class="well">{{$book->book_desc}}</div>
        <table class="table table-striped">
        	<thead>
        		<tr>        			
        		</tr>
        	</thead>
        	<tbody>
        		<tr>
        			<td>Category</td>
        			<td>{{$book->bookcat->category_name}}</td>
        		</tr>
        		<tr>
        			<td>Status</td>
        			<td>{{$book->book_status}}</td>
        		</tr>
        	</tbody>
        </table>
        <h2>Borrow / Return this book</h2>

        @if($book->book_status == 'available')
            {!! Form::open(['action' => ['BookController@borrow',$book->id], 'method' => 'POST']) !!}		            
    			{{Form::submit('Borrow', ['class' => 'btn btn-primary'])}}
            {!! Form::close() !!}
		@elseif(auth()->user()->id == $lastest_user->user->id)
		    {!! Form::open(['action' => ['BookController@return',$book->id], 'method' => 'POST']) !!}
			    {{Form::submit('Return', ['class' => 'btn btn-warning'])}}
            {!! Form::close() !!}
        @else
            <button type="button" class="btn btn-primary" disabled>Not Availble</button>
		@endif
		@endif

        <h2>Borrow log</h2>
        <table class="table table-striped">
        	<thead class="thead-light">
        		<tr>
        			<th>Borrow by</th>
        			<th>Borrow date</th>
                    <th>Due date</th>
        			<th>Return date</th>
        		</tr>        			        	
        	</thead>
        	<tbody>
        		@if($bookslog)
        			@foreach($bookslog as $booklog)
        			<tr>
        				<td>{{$booklog->user->name}}</td>
        				<td>{{\Carbon\Carbon::parse($booklog->borrow_date)->format('d/m/Y')}}</td>
                        <td>{{\Carbon\Carbon::parse($booklog->due_date)->format('d/m/Y')}}</td>
        				<td>{{$booklog->return_date ? \Carbon\Carbon::parse($booklog->return_date)->format('d/m/Y') : null}}</td>
        			</tr>
        			@endforeach        
 				@endif
        	</tbody>
        </table>

@endsection