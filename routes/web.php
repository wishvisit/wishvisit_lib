<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','BookController@index');
Route::group(['middleware' => 'auth' ], function(){
    Route::get('/', [
        'as' => 'home', 'uses' => 'PagesController@index'
    ]);
});
Route::get('/category', 'BookController@category');

Route::resource('/book','BookController');
Route::post('/book/borrow/{id}', 'BookController@borrow');
Route::post('/book/return/{id}', 'BookController@return');


Route::resource('/project','ProjectsController');
Route::resource('/project/project-item','ProjectItemsController');
Route::get('/projectitem/filtered/',[
    'as'   => 'project.showItemFiltered',
    'uses' => 'ProjectItemsController@filter']);
//start={start}&end={end}&type={type}&status={status}

Route::get('/project/project-item/{id}/approve','ProjectItemsController@approve');
Route::get('/project/project-item/{id}/decline','ProjectItemsController@decline');
Route::post('/project/adduser/{project_id}&{user_id}','ProjectsController@add_user');
Route::post('/project/removeuser/{project_id}&{user_id}','ProjectsController@remove_user');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
